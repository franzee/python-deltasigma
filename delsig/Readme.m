% Delta-Sigma Toolbox for Matlab
% Copyright (c) 1993-2020 R. Schreier
% 
% Required Toolboxes:
%   Signal-Processing Toolbox
%   Control Toolbox
% A few non-critical functions also use the Optimization Toolbox.
%
